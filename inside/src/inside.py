import sys
import os
import platform
from tkinter import *
from threading import Thread, Event, Lock
from time import time, sleep
from PIL import Image, ImageTk, ImageDraw, ImageFont
import serial
import RPi.GPIO as GPIO
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from comm.xgk import send_serial, recv_serial  # noqa
# from comm.auto_inside import send_serial, recv_serial

TEST = False

PLC_0: int = 0
PLC_1: int = 1
PLC_2: int = 2
PLC_3: int = 3
PLC_4: int = 4
PLC_5: int = 5
PLC_6: int = 6
PLC_7: int = 7
PLC_8: int = 8
PLC_9: int = 9
PLC_10: int = 10
PLC_11: int = 11

INSIDE_00: int = 0
INSIDE_10: int = 10
INSIDE_11: int = 11
INSIDE_12: int = 12
INSIDE_13: int = 13
INSIDE_20: int = 20
INSIDE_21: int = 21
INSIDE_22: int = 22
INSIDE_23: int = 23
INSIDE_30: int = 30
INSIDE_31: int = 31
INSIDE_32: int = 32
INSIDE_33: int = 33
INSIDE_40: int = 40
INSIDE_41: int = 41
INSIDE_42: int = 42
INSIDE_43: int = 43
INSIDE_50: int = 50
INSIDE_51: int = 51
INSIDE_60: int = 60
INSIDE_61: int = 61
INSIDE_70: int = 70
INSIDE_71: int = 71
INSIDE_80: int = 80
INSIDE_90: int = 90
INSIDE_100: int = 100
INSIDE_110: int = 110

max_plc: int = 7
station: int = 0
address: int = 10310
read_num: int = 1
time_out: float = 1000  # msec
prev_time_flag: float = 0
toggle_time: float = 1500
save_state: int = 0
plc_state: int = -1
plc_car: int = 0
plc_suv: int = 0
count: int = 0
max_num: int = 0
disp_image: int = 0

uart_port: str = '/dev/ttyAMA0'
baud: int = 38400
data_bit: int = serial.EIGHTBITS
stop_bit: int = serial.STOPBITS_ONE
parity: int = serial.PARITY_NONE

screen_w: int = 0
screen_h: int = 0
img_w: int = 0
img_h: int = 0
font_large: int = 0
font_small: int = 0

global win
global img_00
global img_10, img_11, img_12, img_13
global img_20, img_21, img_22, img_23
global img_30, img_31, img_32, img_33
global img_40, img_41, img_42, img_43
global img_50, img_51
global img_60, img_61
global img_70, img_71
global img_80, img_90, img_100, img_110
global label, event, lock, th1, th2

bak_stdout = None


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def disable_print():
    global bak_stdout
    bak_stdout = sys.stdout
    sys.stdout = open(os.devnull, 'w')


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def enable_print():
    global bak_stdout
    if None != bak_stdout:
        sys.stdout = bak_stdout


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def change_num() -> None:
    global plc_state
    global plc_car
    global plc_suv

    plc_state = plc_state + 1
    if max_plc < plc_state:
        # plc_car += 1
        # plc_suv += 1
        plc_state = 0


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def change_image(num: int) -> int:
    global img_00
    global img_10, img_11, img_12, img_13
    global img_20, img_21, img_22, img_23
    global img_30, img_31, img_32, img_33
    global img_40, img_41, img_42, img_43
    global img_50, img_51
    global img_60, img_61
    global img_70, img_71
    global img_80, img_90, img_100, img_110
    global img_w, img_h
    global screen_w, screen_h
    global label
    global font_small, font_large
    font_color: str = 'white'
    message: str = ''

    if INSIDE_00 == num:
        img = img_00
    elif INSIDE_10 == num:
        img = img_10
    elif INSIDE_11 == num:
        img = img_11
    elif INSIDE_12 == num:
        img = img_12
    elif INSIDE_13 == num:
        img = img_13
    elif INSIDE_20 == num:
        img = img_20
    elif INSIDE_21 == num:
        img = img_21
    elif INSIDE_22 == num:
        img = img_22
    elif INSIDE_23 == num:
        img = img_23
    elif INSIDE_30 == num:
        img = img_30
    elif INSIDE_31 == num:
        img = img_31
    elif INSIDE_32 == num:
        img = img_32
    elif INSIDE_33 == num:
        img = img_33
    elif INSIDE_40 == num:
        img = img_40
    elif INSIDE_41 == num:
        img = img_41
    elif INSIDE_42 == num:
        img = img_42
    elif INSIDE_43 == num:
        img = img_43
    elif INSIDE_50 == num:
        img = img_50
    elif INSIDE_51 == num:
        img = img_51
    elif INSIDE_60 == num:
        img = img_60
    elif INSIDE_61 == num:
        img = img_61
    elif INSIDE_70 == num:
        img = img_70
    elif INSIDE_71 == num:
        img = img_71
    elif INSIDE_80 == num:
        img = img_80
    elif INSIDE_90 == num:
        img = img_90
    elif INSIDE_100 == num:
        img = img_100
    elif INSIDE_110 == num:
        img = img_110
    else:
        img = img_00

    print('num 0x%x' % num)
    draw = ImageDraw.Draw(img)

    try:
        if 0 < len(message):
            font_size = font_large
            font1 = ImageFont.truetype('NanumBarunGothicBold.ttf', font_size)
            tx, ty, tw, th = draw.textbbox((0, 0), message, font=font1)
            draw.text(((img_w-tw)/2 + (img_w/3), (img_h-th)/2), message,
                font=font1, fill=font_color, align='center')

        if img_w != screen_w or img_h != screen_h:  # other resolution
            image = img.resize((screen_w, screen_h))
        else:
            image = img
        photo = ImageTk.PhotoImage(image)
    except Exception as e:
        print(f"Exception occurred: {e}")
        return -1

    label.configure(image=photo)
    label.image = photo
    return 0


# ----- THREAD ---------------------------------------------------------------------------------------------------------
def th_display(event_p) -> None:
    global plc_state
    global plc_car
    global plc_suv
    global prev_time_flag
    global toggle_time
    global count
    global max_num
    global disp_image
    disp_state: int = 0
    # disp_car: int = 0
    # disp_suv: int = 0
    plc_change: bool = False

    while True:
        if event_p.is_set():
            print('close th_display')
            return

        lock.acquire(True, 0.1)
        if disp_state != plc_state and 0 <= plc_state :
            disp_state = plc_state
            plc_change = True
        # if disp_car != plc_car and 0 <= plc_car :
        #    disp_car = plc_car
        #    plc_change
        # if disp_suv != plc_suv and 0 <= plc_suv :
        #    disp_suv = plc_suv
        #    plc_change
        lock.release()

        if True == plc_change:
            plc_change = False
            print('state 0x%x' % save_state)

            if PLC_0 == disp_state:
                disp_image = INSIDE_00
                max_num = 1
            elif PLC_1 == disp_state:
                disp_image = INSIDE_10
                max_num = 4
            elif PLC_2 == disp_state:
                disp_image = INSIDE_20
                max_num = 4
            elif PLC_3 == disp_state:
                disp_image = INSIDE_30
                max_num = 4
            elif PLC_4 == disp_state:
                disp_image = INSIDE_40
                max_num = 4
            elif PLC_5 == disp_state:
                disp_image = INSIDE_50
                max_num = 2
            elif PLC_6 == disp_state:
                disp_image = INSIDE_60
                max_num = 2
            elif PLC_7 == disp_state:
                disp_image = INSIDE_70
                max_num = 2
            elif PLC_8 == disp_state:
                disp_image = INSIDE_80
                max_num = 1
            elif PLC_9 == disp_state:
                disp_image = INSIDE_90
                max_num = 1
            elif PLC_10 == disp_state:
                disp_image = INSIDE_100
                max_num = 1
            elif PLC_11 == disp_state:
                disp_image = INSIDE_110
                max_num = 1
            else:
                disp_image = INSIDE_00
                max_num = 1

            count = 0

            if 4 == max_num:
                toggle_time = 250
            else:
                toggle_time = 1500

            prev_time_flag = time() * 1000
            change_image(disp_image)
        else:   # 값이 변경 되지 않는 평상시
            if 1 < max_num:   # 여러장 일때
                now_time_flag: float = time() * 1000
                # print(now_time_flag, prev_time_flag, (now_time_flag-prev_time_flag))
                if toggle_time < (now_time_flag - prev_time_flag) :
                    # print('time %d' % (now_time_flag - prev_time_flag))
                    prev_time_flag = now_time_flag
                    count += 1
                    if max_num <= count:
                        count = 0
                    change_image(disp_image + count)

        sleep(0.01)  # 10ms
    # while


# ----- THREAD ---------------------------------------------------------------------------------------------------------
def th_serial(event_p, port_p: str, baud_p: int, data_bit_p: int,
              stop_bit_p: int, parity_p: int, time_out_p) -> None:
    global plc_state
    global plc_car
    global plc_suv
    global lock

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(20, GPIO.OUT)
    GPIO.setup(26, GPIO.OUT)

    # setup serial
    ser = serial.Serial(port=port_p, baudrate=baud_p, bytesize=data_bit_p, stopbits=stop_bit_p,
                        parity=parity_p, timeout=time_out_p)
    if 0 == ser:
        print('Serial not available')

    while True:
        if event_p.is_set():
            print('close th_serial')
            return

        # SEND
        send_serial(ser, station, address, read_num)
        read_bytes = 13 + (4 * read_num)
        result = recv_serial(ser, read_num, read_bytes, time_out_p)

        lock.acquire(True, 0.1)
        plc_state, plc_car, plc_suv = result
        lock.release()
        # print("%04x %04x %04x" % (plc_state, plc_car, plc_suv))
        if -2 == plc_state:
            GPIO.output(20, False)  # ON
            GPIO.output(26, True)   # OFF
        elif -1 == plc_state:
            GPIO.output(20, True)   # OFF
            GPIO.output(26, False)  # ON
        else:
            GPIO.output(20, True)   # OFF
            GPIO.output(26, True)   # OFF

        sleep(0.01)  # 10ms
    # while


# ----- MAIN -----------------------------------------------------------------------------------------------------------
def main() -> None:
    global screen_w, screen_h
    global img_w, img_h
    global uart_port
    global win
    global img_00
    global img_10, img_11, img_12, img_13
    global img_20, img_21, img_22, img_23
    global img_30, img_31, img_32, img_33
    global img_40, img_41, img_42, img_43
    global img_50, img_51
    global img_60, img_61
    global img_70, img_71
    global img_80, img_90, img_100, img_110
    global label, event, lock, th1, th2
    global font_large, font_small
    font_color: str = 'white'
    message: str = ''

    system_os = platform.system()
    if 'Windows' == system_os:
        uart_port = 'COM18'
    elif 'Linux' == system_os:
        uart_port = '/dev/ttyAMA0'
    else:
        uart_port = '/dev/ttyAMA0'
    print(system_os)

    win = Tk()

    screen_w = win.winfo_screenwidth()
    screen_h = win.winfo_screenheight()
    print(screen_w, screen_h)
    win.geometry(f'{screen_w}x{screen_h}+0+0')
    if True != TEST:
        win.attributes('-fullscreen', True)

    img_00 = Image.open('INSIDE_00.jpg')
    img = img_00
    img_w, img_h = img.size
    draw = ImageDraw.Draw(img)

    if 0 < len(message):
        font_size = 350
        font1 = ImageFont.truetype('NanumBarunGothicBold.ttf', font_size)
        tx, ty, tw, th = draw.textbbox((0, 0), message, font=font1)
        draw.text(((img_w-tw)/2, (img_h-th)/2), message, font=font1, fill=font_color, align='center')

    if img_w != screen_w or img_h != screen_h:  # other resolution
        image = img.resize((screen_w, screen_h))
    else:
        image = img

    photo = ImageTk.PhotoImage(image)
    label = Label(win, image=photo)
    label.pack()

    font_large = 400
    font_small = 350
    img_10 = Image.open('INSIDE_10.jpg')
    img_11 = Image.open('INSIDE_11.jpg')
    img_12 = Image.open('INSIDE_12.jpg')
    img_13 = Image.open('INSIDE_13.jpg')
    img_20 = Image.open('INSIDE_20.jpg')
    img_21 = Image.open('INSIDE_21.jpg')
    img_22 = Image.open('INSIDE_22.jpg')
    img_23 = Image.open('INSIDE_23.jpg')
    img_30 = Image.open('INSIDE_30.jpg')
    img_31 = Image.open('INSIDE_31.jpg')
    img_32 = Image.open('INSIDE_32.jpg')
    img_33 = Image.open('INSIDE_33.jpg')
    img_40 = Image.open('INSIDE_40.jpg')
    img_41 = Image.open('INSIDE_41.jpg')
    img_42 = Image.open('INSIDE_42.jpg')
    img_43 = Image.open('INSIDE_43.jpg')
    img_50 = Image.open('INSIDE_50.jpg')
    img_51 = Image.open('INSIDE_51.jpg')
    img_60 = Image.open('INSIDE_60.jpg')
    img_61 = Image.open('INSIDE_61.jpg')
    img_70 = Image.open('INSIDE_70.jpg')
    img_71 = Image.open('INSIDE_71.jpg')
    try:
        img_80 = Image.open('INSIDE_80.jpg')
    except exception as e:
        img_80 = Image.open('INSIDE_00.jpg')
    try:
        img_90 = Image.open('INSIDE_90.jpg')
    except exceptoon as e:
        img_90 = Image.open('INSIDE_00.jpg')
    try:
        img_100 = Image.open('INSIDE_100.jpg')
    except exceptoon as e:
        img_100 = Image.open('INSIDE_00.jpg')
    try:
        img_110 = Image.open('INSIDE_110.jpg')
    except exceptoon as e:
        img_110 = Image.open('INSIDE_00.jpg')

    # thread
    lock = Lock()
    event = Event()
    th1 = Thread(target=th_serial, args=(event, uart_port, baud, data_bit, stop_bit, parity, time_out))
    th1.daemon = True  # exit with main
    th1.start()

    th2 = Thread(target=th_display, args=(event,))
    th2.daemon = True  # exit with main
    th2.start()

    if True == TEST:
        # exit button
        close = Button(win, text='EXIT', command=quit_program)
        close.place(x=10, y=10)

        # change button
        # change = Button(win, text='CHANGE', command=lambda: change_num())
        change = Button(win, text='CHANGE', command=change_num)
        change.place(x=100, y=10)

    win.mainloop()


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def quit_program() -> None:
    event.set()
    th1.join()
    th2.join()
    win.destroy()
    print('sys.exit')
    sys.exit()


# ----- MAIN -----------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
