from time import time, sleep

data1: int = -1
data2: int = 9
data3: int = 19
prev_time: float = 0
max_plc: int = 7


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def send_serial(ser, station, address, read_num):
    return


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def recv_serial(ser, req_num, req_bytes, time_out):
    global data1, data2, data3, prev_time

    if 0 == data1:  # LOGO after 1500 change others
        diff: float = 1500
    else:
        diff: float = 3000

    curr_time: float = time() * 1000
    if diff <= (curr_time - prev_time):
        prev_time = curr_time
        data1 += 1

        if max_plc < data1:
            data1 = 0

    return data1, data2, data3
