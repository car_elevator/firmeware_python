import sys
import os
import platform
from tkinter import *
from threading import Thread, Event, Lock
from time import time, sleep
from PIL import Image, ImageTk, ImageDraw, ImageFont
import serial
import RPi.GPIO as GPIO
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from comm.xgk import send_serial, recv_serial  # noqa
# from comm.auto_front import send_serial, recv_serial

TEST = False

PLC_1: int = 1
PLC_0: int = 0
PLC_2: int = 2
PLC_3: int = 3
PLC_4: int = 4
PLC_5: int = 5
PLC_6: int = 6
PLC_7: int = 7
PLC_8: int = 8
PLC_9: int = 9
PLC_10: int = 10
PLC_11: int = 11
PLC_12: int = 12
PLC_13: int = 13
PLC_14: int = 14

FRONT_00: int = 0
FRONT_10: int = 10
FRONT_20: int = 20
FRONT_30: int = 30
FRONT_40: int = 40
FRONT_50: int = 50
FRONT_60: int = 60
FRONT_70: int = 70
FRONT_80: int = 80
FRONT_90: int = 90
FRONT_100: int = 100
FRONT_110: int = 110
FRONT_120: int = 120
FRONT_130: int = 130
FRONT_140: int = 140

max_plc: int = 10
station: int = 0
address: int = 10300
read_num: int = 3
time_out: float = 1000  # msec
prev_time_flag: float = 0
toggle_time: float = 1500
plc_state: int = -1
plc_car: int = 0
plc_suv: int = 0
count: int = 0
max_num: int = 0
disp_image: int = 0

uart_port: str = '/dev/ttyAMA0'
baud: int = 38400
dataBit: int = serial.EIGHTBITS
stopBit: int = serial.STOPBITS_ONE
parity: str = serial.PARITY_NONE

screen_w: int = 0
screen_h: int = 0
img_w: int = 0
img_h: int = 0
font_large: int = 0
font_small: int = 0

global win
global img_00, img_10, img_20, img_30
global img_40, img_50, img_60, img_70
global img_80, img_90, img_100, img_110
global img_120, img_130, img_140
global label, event, lock, th1, th2

bak_stdout = None


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def disable_print():
    global bak_stdout
    bak_stdout = sys.stdout
    sys.stdout = open(os.devnull, 'w')


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def enable_print():
    global bak_stdout
    if None != bak_stdout:
        sys.stdout = bak_stdout


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def change_num() -> None:
    global plc_state
    global plc_car
    global plc_suv

    plc_state = plc_state + 1
    if max_plc < plc_state:
        plc_car += 1
        plc_suv += 1
        plc_state = 0


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def change_image(num: int, disp_car: int, disp_suv: int) -> int:
    global img_00, img_10, img_20, img_30
    global img_40, img_50, img_60, img_70
    global img_80, img_90, img_100, img_110
    global img_120, img_130, img_140
    global img_w, img_h
    global screen_w, screen_h
    global label
    global font_small, font_large
    font_color: str = 'white'
    message: str = ''

    if FRONT_00 == num:
        img = img_00
        message = ''
    elif FRONT_10 == num:
        img = img_10
        message = ''
    elif FRONT_20 == num:
        img = img_20
        message = ''
    elif FRONT_30 == num:
        img = img_30
        message = ''
    elif FRONT_40 == num:
        img = img_40
        message = ''
    elif FRONT_50 == num:
        img = img_50
        message = ''
    elif FRONT_60 == num:
        img = img_60
        message = ''
    elif FRONT_70 == num:
        img = img_70
        message = ''
    elif FRONT_80 == num:
        img = img_80
        message = ''
    elif FRONT_90 == num:
        img = img_90.copy()
        message = '승용 %2d 대' % disp_car
    elif FRONT_100 == num:
        img = img_100.copy()
        message = 'SUV %2d 대' % disp_suv
    elif FRONT_110 == num:
        img = img_110
        message = ''
    elif FRONT_120 == num:
        img = img_120
        message = ''
    elif FRONT_130 == num:
        img = img_130
        message = ''
    elif FRONT_140 == num:
        img = img_140
        message = ''
    else:
        img = img_00
        message = ''

    print('num 0x%x %s' % (num, message))
    draw = ImageDraw.Draw(img)

    try:
        if 0 < len(message):
            font_size = font_large
            font1 = ImageFont.truetype('../font/NanumBarunGothicBold.ttf', font_size)
            tx, ty, tw, th = draw.textbbox((0, 0), message, font=font1)
            draw.text(((img_w - tw) / 2 + (img_w / 10), (img_h - th) / 2), message,
                font=font1, fill=font_color, align='center')

        if img_w != screen_w or img_h != screen_h:  # other resolution
            image = img.resize((screen_w, screen_h))
        else:
            image = img
        photo = ImageTk.PhotoImage(image)
    except Exception as e:
        print(f"Exception occurred: {e}")
        return -1

    label.configure(image=photo)
    label.image = photo
    return 0


# ----- THREAD ---------------------------------------------------------------------------------------------------------
def th_display(event_p) -> None:
    global plc_state
    global plc_car
    global plc_suv
    global prev_time_flag
    global toggle_time
    global count
    global max_num
    global disp_image
    disp_state: int = 0
    disp_car: int = 0
    disp_suv: int = 0
    plc_change: bool = False

    while True:
        if event_p.is_set():
            print('close th_display')
            return

        lock.acquire(True, 0.1)
        if disp_state != plc_state and 0 <= plc_state:
            disp_state = plc_state
            plc_change = True
        if disp_car != plc_car and 0 <= plc_car:
            disp_car = plc_car
            plc_change = True
        if disp_suv != plc_suv and 0 <= plc_suv:
            disp_suv = plc_suv
            plc_change = True
        lock.release()

        if True == plc_change:
            plc_change = False
            print('state 0x%x' % disp_state)

            if PLC_0 == disp_state:
                disp_image = FRONT_00
                max_num = 1
            elif PLC_1 == disp_state:
                disp_image = FRONT_10
                max_num = 1
            elif PLC_2 == disp_state:
                disp_image = FRONT_20
                max_num = 1
            elif PLC_3 == disp_state:
                disp_image = FRONT_30
                max_num = 1
            elif PLC_4 == disp_state:
                disp_image = FRONT_40
                max_num = 1
            elif PLC_5 == disp_state:
                disp_image = FRONT_50
                max_num = 1
            elif PLC_6 == disp_state:
                disp_image = FRONT_60
                max_num = 1
            elif PLC_7 == disp_state:
                disp_image = FRONT_70
                max_num = 1
            elif PLC_8 == disp_state:
                disp_image = FRONT_80
                max_num = 1
            elif PLC_9 == disp_state:
                disp_image = FRONT_90
                max_num = 1
            elif PLC_10 == disp_state:
                disp_image = FRONT_100
                max_num = 1
            elif PLC_11 == disp_state:
                disp_image = FRONT_110
                max_num = 1
            elif PLC_12 == disp_state:
                disp_image = FRONT_120
                max_num = 1
            elif PLC_13 == disp_state:
                disp_image = FRONT_130
                max_num = 1
            elif PLC_14 == disp_state:
                disp_image = FRONT_140
                max_num = 1
            else:
                disp_image = FRONT_00
                max_num = 1

            count = 0

            prev_time_flag = time() * 1000
            change_image(disp_image, disp_car, disp_suv)
        else:  # 값이 변경 되지 않는 평상시
            if 1 < max_num:  # 여러장 일때
                now_time_flag = time() * 1000
                # print(now_time_flag, prev_time_flag, (now_time_flag-prev_time_flag))
                if toggle_time < (now_time_flag - prev_time_flag):
                    prev_time_flag = now_time_flag
                    count += 1
                    if max_num <= count:
                        count = 0
                    change_image(disp_image + count, disp_car, disp_suv)

        sleep(0.01)  # 10ms
    # while


# ----- THREAD ---------------------------------------------------------------------------------------------------------
def th_serial(event_p, port_p: str, baud_p: int, data_bit_p: int,
              stop_bit_p: int, parity_p: str, time_out_p: int) -> None:
    global plc_state
    global plc_car
    global plc_suv
    global lock

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(20, GPIO.OUT)
    GPIO.setup(26, GPIO.OUT)

    # setup serial
    ser = serial.Serial(port=port_p, baudrate=baud_p, bytesize=data_bit_p, stopbits=stop_bit_p, parity=parity_p,
                        timeout=time_out_p)
    if 0 == ser:
        print('Serial not available')

    while True:
        if event_p.is_set():
            print('close th_serial')
            return

        # SEND
        send_serial(ser, station, address, read_num)
        req_bytes = 13 + (4 * read_num)
        result = recv_serial(ser, read_num, req_bytes, time_out)

        lock.acquire(True, 0.1)
        plc_state, plc_car, plc_suv = result
        lock.release()
        # print("%04x %04x %04x" % (plc_state, plc_car, plc_suv))
        if -2 == plc_state:
            GPIO.output(20, False)  # ON
            GPIO.output(26, True)   # OFF
            sleep(0.1)
        elif -1 == plc_state:
            GPIO.output(20, True)   # OFF
            GPIO.output(26, False)  # ON
            sleep(0.1)
        else:
            GPIO.output(20, True)  # OFF
            GPIO.output(26, True)  # OFF

        sleep(0.01)  # 10ms
    # while


# ----- MAIN PROGRAM ---------------------------------------------------------------------------------------------------
def main() -> None:
    global screen_w, screen_h
    global img_w, img_h
    global uart_port
    global win
    global img_00, img_10, img_20, img_30
    global img_40, img_50, img_60, img_70
    global img_80, img_90, img_100, img_110
    global img_120, img_130, img_140
    global label, event, lock, th1, th2
    global font_large, font_small
    font_color: str = 'white'
    message: str = ''

    system_os = platform.system()
    if 'Windows' == system_os:
        uart_port = 'COM18'
    elif 'Linux' == system_os:
        uart_port = '/dev/ttyAMA0'
    else:
        uart_port = '/dev/ttyAMA0'
    print(system_os)

    win = Tk()

    screen_w = win.winfo_screenwidth()
    screen_h = win.winfo_screenheight()
    print(screen_w, screen_h)
    win.geometry(f'{screen_w}x{screen_h}+0+0')
    if True != TEST:
        win.attributes('-fullscreen', True)

    img_00 = Image.open('FRONT_00.jpg')
    img = img_00
    img_w, img_h = img.size

    draw = ImageDraw.Draw(img)

    if 0 < len(message):
        font_size = 350
        font1 = ImageFont.truetype('font/NanumBarunGothicBold.ttf', font_size)
        tx, ty, tw, th = draw.textbbox((0, 0), message, font=font1)
        draw.text(((img_w - tw) / 2, (img_h - th) / 2), message, font=font1, fill=font_color, align='center')

    if img_w != screen_w or img_h != screen_h:  # other resolution
        image = img.resize((screen_w, screen_h))
    else:
        imgae = img

    photo = ImageTk.PhotoImage(image)
    label = Label(win, image=photo)
    label.pack()

    font_large = 320
    font_small = 300
    img_10 = Image.open('FRONT_10.jpg')
    img_20 = Image.open('FRONT_20.jpg')
    img_30 = Image.open('FRONT_30.jpg')
    img_40 = Image.open('FRONT_40.jpg')
    img_50 = Image.open('FRONT_50.jpg')
    img_60 = Image.open('FRONT_60.jpg')
    img_70 = Image.open('FRONT_70.jpg')
    img_80 = Image.open('FRONT_80.jpg')
    img_90 = Image.open('FRONT_90.jpg')
    img_100 = Image.open('FRONT_100.jpg')
    try:
        img_110 = Image.open('FRONT_110.jpg')
    except Exception as e:
        img_110 = Image.open('FRONT_00.jpg')
    try:
        img_120 = Image.open('FRONT_120.jpg')
    except Exception as e:
        img_120 = Image.open('FRONT_00.jpg')
    try:
        img_130 = Image.open('FRONT_130.jpg')
    except Exception as e:
        img_130 = Image.open('FRONT_00.jpg')
    try:
        img_140 = Image.open('FRONT_140.jpg')
    except Exception as e:
        img_140 = Image.open('FRONT_00.jpg')

    # thread
    lock = Lock()
    event = Event()
    th1 = Thread(target=th_serial, args=(event, uart_port, baud, dataBit, stopBit, parity, time_out))
    th1.daemon = True  # exit with main
    th1.start()

    th2 = Thread(target=th_display, args=(event,))
    th2.daemon = True  # exit with main
    th2.start()

    if True == TEST:
        # exit button
        close = Button(win, text='EXIT', command=quit_program)
        close.place(x=10, y=10)

        # change button
        # change = Button(win, text='CHANGE', command=lambda: change_num())
        change = Button(win, text='CHANGE', command=change_num)
        change.place(x=100, y=10)

    win.mainloop()


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def quit_program() -> None:
    global event, th1, th2, win

    event.set()
    th1.join()
    th2.join()
    win.destroy()
    print('sys.exit')
    sys.exit()


# ----- MAIN -----------------------------------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
