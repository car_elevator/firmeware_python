from time import time, sleep

ENQ = 0x05
EOT = 0x04
ACK = 0x06
NAK = 0x15
ETX = 0x03

tx_buf = bytearray(100)
rx_buf = bytearray(100)


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def signed_int(num: int):
    if (num & 0x8000) == 0x8000:
        num = -((num ^ 0xffff) + 1)
    return num


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def send_serial(ser, station: int, address: int, read_num: int):
    global tx_buf
    global rx_buf

    tx_buf.clear()
    tx_buf.append(ENQ)
    stn = (str("%02X" % station).encode('ASCII'))
    tx_buf.append(stn[0])
    tx_buf.append(stn[1])
    tx_buf.append(ord('r'))
    tx_buf.append(ord('S'))
    tx_buf.append(ord('B'))
    tx_buf.append(ord('0'))
    tx_buf.append(ord('8'))
    tx_buf.append(ord('%'))
    tx_buf.append(ord('D'))
    tx_buf.append(ord('W'))
    addr = (str("%05d" % address).encode('ASCII'))
    tx_buf.append(addr[0])
    tx_buf.append(addr[1])
    tx_buf.append(addr[2])
    tx_buf.append(addr[3])
    tx_buf.append(addr[4])
    num = (str("%02X" % read_num).encode('ASCII'))
    tx_buf.append(num[0])
    tx_buf.append(num[1])
    tx_buf.append(EOT)
    checksum = sum(tx_buf) & 0xff
    bcc = (str("%02X" % checksum).encode('ASCII'))
    tx_buf.append(bcc[0])
    tx_buf.append(bcc[1])

    ser.flushInput()
    ser.write(tx_buf)
    print('\n>send %d' % len(tx_buf))

    return


# ----- FUNCTION -------------------------------------------------------------------------------------------------------
def recv_serial(ser, req_num: int, req_bytes: int, time_out: float):
    global tx_buf
    global rx_buf

    send_time: float = time() * 1000
    rx_bytes: int = 0
    pos_start: int = -1
    pos_end: int = -1
    data1: int = 0
    data2: int = 0
    data3: int = 0

    rx_buf.clear()

    while True:
        length = ser.inWaiting()
        if 0 < length:
            print(f'length {length}')
            buf = ser.read(length)
            rx_buf += buf
            rx_bytes += length

            if rx_bytes >= req_bytes : # and -1 != pos_start and -1 != pos_end :
                pos_start = rx_buf.find(ACK)
                pos_end = rx_buf.find(ETX)
                if -1 != pos_start and -1 != pos_end:
                    data = rx_buf[pos_start:pos_end+1]
                    calc_sum = sum(data) & 0xff
                    recv_sum = int(rx_buf[pos_end+1:pos_end+2+1], 16)
                    if recv_sum != calc_sum:
                        print('check sum error %02x %02x' % (calc_sum, recv_sum))
                        return -1, 0, 0
                    print(data)
                    print('found')
                    break
                elif -1 == pos_start:
                    pos_nak = rx_buf.find(NAK)
                    if -1 != pos_nak:
                        print('NAK')
                        return -1, 0, 0

        recv_time: float = time() * 1000
        if time_out < (recv_time - send_time):
            print('TIME OUT')
            return -2, 0, 0

        sleep(0.01)

    print('recv %d' % len(rx_buf))
    print('station %d' % int(data[1:2+1], 16)) # station
    if b'rSB' != data[3:5+1]:  # rSB
        print('rSB error')
        return -1, 0, 0
    if 1 != int(data[6:7+1], 16):  # num of block
        print('block error')
        return -1, 0, 0
    if req_num != int(data[8:9+1], 16) / 2:  # data bytes
        print('data bytes error')
        return -1, 0, 0
    if 1 <= req_num:
        data1 = int(data[10:13+1], 16)
    if 2 <= req_num:
        data2 = int(data[14:17+1], 16)
    if 3 <= req_num:
        data3 = int(data[18:21+1], 16)

    print('0x%04X' % data1)
    print('0x%04X' % data2)
    print('0x%04X' % data3)
    print('end of receive')
    return signed_int(data1), signed_int(data2), signed_int(data3)
