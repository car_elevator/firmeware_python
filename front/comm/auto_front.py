from time import time, sleep

data1: int = -1
data2: int = 9
data3: int = 19
prev_time: float = 0
max_plc: int = 10
diff: float = 1500


def send_serial(ser, station: int, address: int, read_num: int):
    return


def recv_serial(ser, req_num: int, req_bytes: int, time_out: float):
    global data1, data2, data3, prev_time, max_plc, diff

    # if 0 == data1 : # PLC_LOGO after 1500 change others
    #    diff = 1500
    # elif  1 == data1 : # PLC_CAR
    #    diff = 6000
    # else :
    #    diff = 1500

    curr_time: float = time() * 1000
    if diff <= (curr_time - prev_time):
        prev_time = curr_time
        data1 += 1
        if 0 == data1:
            data2 += 1
            data3 += 1

        if max_plc < data1:
            data1 = 0
        if 99 < data2:
            data2 = 0
        if 99 < data3:
            data3 = 0

    return data1, data2, data3
