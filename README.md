# 오텍 오티스 프로젝트

## 1. front : 기계식 주차장의 도어 위쪽에 14인치 Bar LCD.

- comm 폴더 :
    - xgk.py : PLC와 통신하는 파일
    - auto_front.py : 전시용으로  자동으로 PLC값을 증가시키는 파일
- font 폴더
    - NanumBarunGothicBold.ttf : 화면에 한글 메시지 표시
- src 폴더
    - front.py : 프로그램 시작 파일, PLC에서 읽어온 데이터에 맞는 이미지를 화면에 표시하는 프로그램
- 실행방법
    - python front.py

## inside : 기계식 주차장의 내부 거울 위쪽에 있는 24인치 FHD LCD.

- comm 폴더 :
    - xgk.py : PLC와 통신하는 파일
    - auto_inside.py : 전시용으로  자동으로 PLC값을 증가시키는 파일
- font 폴더
    - NanumBarunGothicBold.ttf : 화면에 한글 메시지 표시
- src 폴더
    - inside.py : 프로그램 시작 파일, PLC에서 읽어온 데이터에 맞는 이미지를 화면에 표시하는 프로그램
- 실행방법
    - python inside.py